package selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;

public class Topic07_IFrame_Window_Handle {
	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js;
	Actions act;
	  @BeforeTest
	  public void beforeTest() {
		  driver=new FirefoxDriver();
		  wait=new WebDriverWait(driver,5);
		  js=(JavascriptExecutor)driver;
		  act=new Actions(driver);
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.manage().window().maximize();
		  
		 
	  }
	  
	  public boolean isIFrameDisplayed(List<WebElement> iFrame) {
		  if (iFrame.size()>0)
			  return true;
		  else return false;
	  }
	  
	  public boolean isImgDisplayed(WebElement img) {
			  JavascriptExecutor js=(JavascriptExecutor) driver;
			  Object result=js.executeScript( "return arguments[0].complete && "+
					   "typeof arguments[0].naturalWidth != \"undefined\" && "+
					   "arguments[0].naturalWidth > 0", img);		
			return (boolean) result;
		 
	  }
	/*  @Test
	  public void TC01_iFrameHandle() throws InterruptedException {		
		  System.out.println("Excecise1:");
		  //step1
		  driver.get("https://www.hdfcbank.com/");		 
		  //step2
		  List<WebElement> iFramePopup=driver.findElements(By.id("vizury-notification-template"));
		  if(isIFrameDisplayed(iFramePopup)) {
			  driver.switchTo().frame(iFramePopup.get(0));
			  js.executeScript("arguments[0].click();", driver.findElement(By.id("div-close")));
			  
			  System.out.println("IFrame vizury-notification-template is closed");
		  }
		  else System.out.println("IFrame vizury-notification-template is not displayed");
		  //step3
		  driver.switchTo().defaultContent();
		  List<WebElement> iFrameMsgHeader=driver.findElements(By.xpath("//iframe[contains(@id,'viz_iframe') and @width='522']"));		 
			  driver.switchTo().frame(iFrameMsgHeader.get(0));		  
			  Assert.assertEquals(driver.findElement(By.id("messageText")).getText(), "What are you looking for?");	
			  System.out.println("What are you looking for? is correct");
		  //step4
		  driver.switchTo().defaultContent();
		  List<WebElement> iFrameBanner=driver.findElements(By.xpath("//iframe[contains(@id,'viz_iframe') and @width='373']"));		  
		  driver.switchTo().frame(iFrameBanner.get(0));
		  List<WebElement> imgList1=new ArrayList(driver.findElements(By.xpath("//div[@id='productcontainer']//img[@class='bannerimage']")));
		  Assert.assertEquals(imgList1.size(), 6);
		  int i=0;
		  for(WebElement img:imgList1) {
				  driver.findElement(By.id("thumbnail-"+i)).click();
				  //wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("thumbnail-"+i)));
				  Assert.assertTrue(isImgDisplayed(img));
				  System.out.println("Image Banner "+i+" is fully loaded");
				  i++;
			  }
		  i=0;
		  //step5
		  driver.switchTo().defaultContent();
		  List<WebElement> imgList2=new ArrayList(driver.findElements(By.xpath("//div[@class='flipBanner']//div/img[@class='front icon']")));
		  Assert.assertEquals(imgList2.size(), 8);
		  for(WebElement img:imgList2) {
			  Assert.assertTrue(isImgDisplayed(img));
			  System.out.println("Image"+i+" is fully loaded");
			  i++;
		  }
	  }*/
		  
	  @Test
	  public void TC02_windowsHandle() throws InterruptedException {		
		  System.out.println("Excecise2:");
		  //step1
		  driver.get("https://daominhdam.github.io/basic-form/index.html");		 
		  //step2
		  js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[text()='Click Here']")));
		  //Step3
		  String parentWindow=driver.getWindowHandle();
		  Set<String> allWindows=driver.getWindowHandles();
		  for(String childWindow:allWindows) {
			  if(!childWindow.equals(parentWindow)) {
				  driver.switchTo().window(childWindow);
				  Assert.assertEquals(driver.getTitle(),"Google");
				  driver.close();
				  break;
			  }
		  }
		  driver.switchTo().window(parentWindow) ; 
		  Assert.assertEquals(driver.getTitle(),"SELENIUM WEBDRIVER FORM DEMO");
	  }
}
