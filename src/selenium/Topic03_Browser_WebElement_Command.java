package selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;

public class Topic03_Browser_WebElement_Command {
	WebDriver driver;
	
	  @BeforeTest
	  public void beforeTest() {
		  driver=new FirefoxDriver();
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.manage().window().maximize();
		  driver.get("https://daominhdam.github.io/basic-form/index.html");
	  }

	  @Test
	  public void TC01_checkDisplayElement() throws InterruptedException {
		  WebElement email=driver.findElement(By.id("mail"));
		  WebElement ageUnder18=driver.findElement(By.id("under_18"));
		  WebElement edu=driver.findElement(By.id("edu"));
		  if(email.isDisplayed()) {
			  email.sendKeys("Automation Testing");
		  }
		  if(ageUnder18.isDisplayed()) {
			  ageUnder18.click();
		  }
		  if(edu.isDisplayed()) {
			  edu.sendKeys("Automation Testing");
		  }		 
	  }
	  
	  @Test
	  public void TC02_CheckElementEnable() throws InterruptedException {
		  System.out.println("*Testcase02*");
		  ArrayList<WebElement> listElement=new ArrayList<WebElement>();
		  listElement.add(driver.findElement(By.id("mail")));
		  listElement.add(driver.findElement(By.id("password")));
		  listElement.add(driver.findElement(By.id("under_18")));
		  listElement.add(driver.findElement(By.id("radio-disabled")));
		  listElement.add(driver.findElement(By.id("edu")));
		  listElement.add(driver.findElement(By.id("bio")));
		  listElement.add(driver.findElement(By.id("job1")));
		  listElement.add(driver.findElement(By.id("job2")));
		  listElement.add(driver.findElement(By.id("development")));
		  listElement.add(driver.findElement(By.id("slider-1")));
		  listElement.add(driver.findElement(By.id("slider-2")));
		  listElement.add(driver.findElement(By.id("button-enabled")));
		  listElement.add(driver.findElement(By.id("button-disabled")));
		  
		  for(WebElement element:listElement) {
			  if(element.isEnabled()) {
				  System.out.println(element.getAttribute("id")+" is enabled");
			  }else System.out.println(element.getAttribute("id")+" is disabled");
		  }
		  
	  }
	  @Test
	  public void TC03_checkElementSelected() throws InterruptedException {
		  System.out.println("*Testcase03*");
		  WebElement ageUnder18=driver.findElement(By.id("under_18"));
		  WebElement interest_dev=driver.findElement(By.id("development"));
		  ageUnder18.click();
		  interest_dev.click();
		  if(interest_dev.isSelected()) {
			  System.out.println(interest_dev.getAttribute("id")+" is selected");
		  }
		  else  interest_dev.click();
		  
		  if(ageUnder18.isSelected()) {
			  System.out.println(ageUnder18.getAttribute("id")+" is selected");
		  }
		  else  ageUnder18.click();
	  }
	  
}
