package selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;

public class Topic04_DropDown_Handle {
	WebDriver driver;
	
	  @BeforeTest
	  public void beforeTest() {
		  driver=new FirefoxDriver();
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.manage().window().maximize();
		 
	  }
	/*  @Test
	  public void TC01_handleHTMLDropdown() throws InterruptedException {
		  driver.get("https://daominhdam.github.io/basic-form/index.html");
		  Thread.sleep(2000);
		  WebElement job1=driver.findElement(By.id("job1"));
		  Select job1Selector= new Select(job1);
		  //step 2
		  Assert.assertFalse(job1Selector.isMultiple());
		  //Step 3
		  job1Selector.selectByVisibleText("Automation Tester");
		  //Step 4
		  Assert.assertEquals(job1Selector.getFirstSelectedOption().getText(), "Automation Tester");
		  //Step 5
		  job1Selector.selectByValue("manual");
		  //Step 6
		  Assert.assertEquals(job1Selector.getFirstSelectedOption().getText(), "Manual Tester");
		  //Step 7
		  job1Selector.selectByIndex(3);
		  //Step 8
		  Assert.assertEquals(job1Selector.getFirstSelectedOption().getText(), "Mobile Tester");
		  //Step 9
		  for(WebElement option:job1Selector.getOptions()) {
			  System.out.println(option.getText());
		  }
	  }
	  */
	  
	  public void selectOption(By listboxLocator, By listLocator, String expectedoption) {
		  JavascriptExecutor js=(JavascriptExecutor)driver;
		  js.executeScript("arguments[0].click();", driver.findElement(listboxLocator));
		 // driver.findElement(listboxLocator).click();
		  WebDriverWait wait=new WebDriverWait(driver,5);
		  wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(listLocator)));
		  List<WebElement> optionList=driver.findElements(listLocator);
		  for(WebElement option:optionList) {
			  
			  js.executeScript("arguments[0].scrollIntoView();", option);
			  if(option.getText().equals(expectedoption)) {							 
					  option.click();
					  System.out.println(expectedoption+" is selected");
					 break;
			  }
		  }
		 
	  }
	  
	  
	  @Test
	  public void TC02_handleCustomDropdown() throws InterruptedException {		
		  
		  driver.get("http://jqueryui.com/resources/demos/selectmenu/default.html");
		  selectOption(By.cssSelector("#number-button span"),By.cssSelector("#number-menu li div"), "19");
		  //Verify option "19" is selected
		  Assert.assertEquals(driver.findElement(By.cssSelector("#number-button .ui-selectmenu-text")).getText(), "19");
	  }
	  
	  @Test
	  public void TC03_Sample01() throws InterruptedException {		
		  
		  driver.get("https://material.angular.io/components/select/examples");
		  selectOption(By.xpath("//span[text()='State']"),By.xpath("//div[@id='cdk-overlay-0']//mat-option"), "Texas");
		  //Verify option "19" is selected
		  Assert.assertEquals(driver.findElement(By.xpath("//mat-select[@aria-label='State']//span/span")).getText(), "Texas");
	  }
	  
	  @Test
	  public void TC03_Sample02() throws InterruptedException {		
		  
		  driver.get("https://demos.telerik.com/kendo-ui/dropdownlist/index");
		  selectOption(By.xpath("//span[@aria-owns='color_listbox']"),By.xpath("//ul[@id='color_listbox']/li"), "Orange");
		  //Verify option "19" is selected
		  Assert.assertEquals(driver.findElement(By.xpath("//span[@aria-owns='color_listbox']/span/span")).getText(), "Orange");
	  }
	  
}
