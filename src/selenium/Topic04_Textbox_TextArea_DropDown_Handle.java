package selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;

public class Topic04_Textbox_TextArea_DropDown_Handle {
	WebDriver driver;
	By newCustomerLink=By.xpath("//a[text()='New Customer']");
	By editCustomerLink=By.xpath("//a[text()='Edit Customer']");
	By delCustomerLink=By.xpath("//a[text()='Delete Customer']");
	
	//New Customer form
	By name=By.name("name");
	By gender=By.name("gender");
	By genderMale=By.xpath("//input[@name='rad1' and @value='m']");
	By genderFemale=By.xpath("//input[@name='rad1' and @value='f']");
	By dob=By.name("dob");
	By address=By.name("addr");
	By city=By.name("city");
	By state=By.name("state");
	By pin=By.name("pinno");
	By phone=By.name("telephoneno");
	By email=By.name("emailid");
	By pass=By.name("password");
	By submit=By.name("sub");
	By reset=By.name("res");
	
	//default value for create new customer
	String nameVal="Hue";
	String dobVal="09/14/1990";
	String dobValAfter="1990-09-14";
	String genderVal="male";
	String addVal="59 Me Tri";
	String cityVal="Hanoi";
	String stateVal="Vietnam";
	String pinVal="012345";
	String phoneVal="0978432534";
	String emailVal="test"+Math.floor(Math.random()*100)+"@gmail.com";
	String passVal="admin123";
	
	//Edit Customer Form
	By cusID=By.name("cusid");
	By accSubmit=By.name("AccSubmit");
	
	//default value for Update customer
	String addValEdit="59 Me Tri Ha";
	String cityValEdit="Saigon";
	String stateValEdit="Viet nam";
	String pinValEdit="012367";
	String phoneValEdit="0978432589";
	String emailValEdit="test"+Math.floor(Math.random()*100)+"@gmail.com";
	
	public boolean isEnabled(By locator) {
		if(driver.findElement(locator).isEnabled())
			return true;
		else return false;
	}
	
	
	  @BeforeTest
	  public void beforeTest() {
		  driver=new FirefoxDriver();
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.manage().window().maximize();
		 
	  }

	  @Test
	  public void TC01_handleTextboxTextArea() throws InterruptedException {
		  driver.get("http://demo.guru99.com/v4");
		  driver.findElement(By.name("uid")).sendKeys("mngr181358");
		  driver.findElement(By.name("password")).sendKeys("berydUp");
		  driver.findElement(By.name("btnLogin")).click();
		//Step 4-Create new customer
		  driver.findElement(newCustomerLink).click();
		 driver.findElement(name).sendKeys(nameVal);
		 driver.findElement(dob).sendKeys(dobVal);
		 driver.findElement(address).sendKeys(addVal);
		 driver.findElement(city).sendKeys(cityVal);
		 driver.findElement(state).sendKeys(stateVal);
		 driver.findElement(pin).sendKeys(pinVal);
		 driver.findElement(phone).sendKeys(phoneVal);
		 driver.findElement(email).sendKeys(emailVal);
		 driver.findElement(pass).sendKeys(passVal);
		 driver.findElement(submit).click();
		 Thread.sleep(3000);
		 
		 //Step 5-Get Customer ID
		 String customerID=driver.findElement(By.xpath("//td[text()='Customer ID']/following-sibling::td")).getText();
		 System.out.println(customerID);
		 //Step 6-Verify info
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Customer Name']/following-sibling::td")).getText(), nameVal);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Gender']/following-sibling::td")).getText(), genderVal);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Birthdate']/following-sibling::td")).getText(), dobValAfter);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Address']/following-sibling::td")).getText(), addVal);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='City']/following-sibling::td")).getText(), cityVal);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='State']/following-sibling::td")).getText(), stateVal);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Pin']/following-sibling::td")).getText(), pinVal);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Mobile No.']/following-sibling::td")).getText(), phoneVal);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Email']/following-sibling::td")).getText(), emailVal);
		
		//Step 7-Edit customer
		 driver.findElement(editCustomerLink).click();
		 driver.findElement(cusID).sendKeys(customerID);
		 driver.findElement(accSubmit).click();
		//Step 8-Verify name and address
		 Assert.assertEquals(driver.findElement(name).getAttribute("value"), nameVal);
		 Assert.assertEquals(driver.findElement(address).getText(), addVal);
		 //Step 9-Update information
		 Assert.assertFalse(isEnabled(name));
		 Assert.assertFalse(isEnabled(gender));
		 Assert.assertFalse(isEnabled(dob));
		
		 if(isEnabled(address)) {
			 driver.findElement(address).clear();
			 driver.findElement(address).sendKeys(addValEdit);
		 }
		 if(isEnabled(city)) {
			 driver.findElement(city).clear();
			 driver.findElement(city).sendKeys(cityValEdit);
		 }
		 if(isEnabled(state)) {
			 driver.findElement(state).clear();
			 driver.findElement(state).sendKeys(stateValEdit);
		 }
		 if(isEnabled(pin)) {
			 driver.findElement(pin).clear();
			 driver.findElement(pin).sendKeys(pinValEdit);
		 }
		 if(isEnabled(phone)) {
			 driver.findElement(phone).clear();
			 driver.findElement(phone).sendKeys(phoneValEdit);
		 }
		 if(isEnabled(email)) {
			 driver.findElement(email).clear();
			 driver.findElement(email).sendKeys(emailValEdit);
		 }
		 driver.findElement(submit).click();
		 //Step 10-verify edit information
		 Thread.sleep(2000);
		 Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Customer Name']/following-sibling::td")).getText(), nameVal);
			Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Gender']/following-sibling::td")).getText(), genderVal);
			Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Birthdate']/following-sibling::td")).getText(), dobValAfter);
			Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Address']/following-sibling::td")).getText(), addValEdit);
			Assert.assertEquals(driver.findElement(By.xpath("//td[text()='City']/following-sibling::td")).getText(), cityValEdit);
			Assert.assertEquals(driver.findElement(By.xpath("//td[text()='State']/following-sibling::td")).getText(), stateValEdit);
			Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Pin']/following-sibling::td")).getText(), pinValEdit);
			Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Mobile No.']/following-sibling::td")).getText(), phoneValEdit);
			Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Email']/following-sibling::td")).getText(), emailValEdit);
		 
	  }
	  
}
