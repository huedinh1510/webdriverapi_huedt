package selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;

public class Topic08_JavaScript {
	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor js;
	  @BeforeTest
	  public void beforeTest() {
		  driver=new ChromeDriver();
		  wait=new WebDriverWait(driver,5);
		  js=(JavascriptExecutor)driver;
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.manage().window().maximize();
		  
		 
	  }
	  
	  @Test
	  public void TC01_JsExecutor() throws InterruptedException {		
		  System.out.println("Excecise1:");
		  //step1
		  driver.get("http://live.guru99.com/");		 
		  //step2
		  String domain=(String)js.executeScript("return document.domain;");
		  Assert.assertEquals(domain, "live.guru99.com");
		  //Step 3
		  String URL=(String)js.executeScript("return document.URL;");
		  Assert.assertEquals(URL, "http://live.guru99.com/");
		  //Step 4
		  js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[text()='Mobile']")));
		//Step 5
		  js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[text()='Samsung Galaxy']/following::span[text()='Add to Cart']")));
		  Assert.assertEquals(URL, "http://live.guru99.com/");
		  //Step6
		  String innerText=(String)js.executeScript("return document.documentElement.getInnerText();");
		  Assert.assertTrue(innerText.contains("Samsung Galaxy was added to your shopping cart."));
		  //step7
		  js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[text()='Privacy Policy']")));
		  String title=(String)js.executeScript("return document.title");
		  Assert.assertEquals(title, "Privacy Policy");
		  //step8
		  WebElement wishlist_cnt=driver.findElement(By.xpath("//th[text()='WISHLIST_CNT']/following-sibling::td"));
		  js.executeScript("window.scrollBy(0,document.body.scrollHeight);");
		  //Step9
		  Assert.assertTrue(wishlist_cnt.isDisplayed());
		  //Step10
		  js.executeScript("window.location=\"http://demo.guru99.com/v4/\"");
		  String newDomain=(String)js.executeScript("return document.domain;");
		  Assert.assertEquals(newDomain, "demo.guru99.com");
		  
	  }
	  
	  @Test
	  public void TC02_removeAttribute() throws InterruptedException {		
		  System.out.println("Excecise2:");
		  //step1
		  driver.get("http://demo.guru99.com/v4");		 
		  //step2
		 driver.findElement(By.name("uid")).sendKeys("mngr181358");
		 driver.findElement(By.name("password")).sendKeys("berydUp");
		 driver.findElement(By.name("btnLogin")).click();
		 //Step3
		 driver.findElement(By.xpath("//a[text()='New Customer']")).click();
		 js.executeScript("arguments[0].removeAttribute('type');", driver.findElement(By.name("dob")));
		 driver.findElement(By.name("dob")).sendKeys("10/10/1990");
		 driver.findElement(By.name("name")).sendKeys("hue");
		 driver.findElement(By.name("addr")).sendKeys("106 HQV");
		 driver.findElement(By.name("city")).sendKeys("Hanoi");
		 driver.findElement(By.name("state")).sendKeys("Vietnam");
		 driver.findElement(By.name("pinno")).sendKeys("123456");
		 driver.findElement(By.name("telephoneno")).sendKeys("019382754576");
		 driver.findElement(By.name("emailid")).sendKeys("hue112@yopmail.com");
		 driver.findElement(By.name("password")).sendKeys("abc123!@");
		 //driver.findElement(By.name("sub")).click();
		 //Step4
		 
	  }
	  
	  public void sendkeysByJS(WebElement element, String value) {
		  js.executeScript("arguments[0].setAttribute('value','" + value + "')", element);
	  }
	
	  
	  @Test
	  public void TC03_sendkeysAttribute() throws InterruptedException {		
		  System.out.println("Excecise3:");
		  //step1
		  driver.get("http://demo.guru99.com/v4");		 
		  //step2
		 driver.findElement(By.name("uid")).sendKeys("mngr181358");
		 driver.findElement(By.name("password")).sendKeys("berydUp");
		 driver.findElement(By.name("btnLogin")).click();
		 //Step3
		 driver.findElement(By.xpath("//a[text()='New Customer']")).click();
		 sendkeysByJS(driver.findElement(By.name("name")),"hue");
		 sendkeysByJS(driver.findElement(By.name("dob")),"10/10/1990");
		 sendkeysByJS(driver.findElement(By.name("addr")),"106 HQV");
		 sendkeysByJS(driver.findElement(By.name("city")),"Hanoi");
		 sendkeysByJS(driver.findElement(By.name("state")),"Vietnam");
		 sendkeysByJS(driver.findElement(By.name("pinno")),"123456V");
		 sendkeysByJS(driver.findElement(By.name("telephoneno")),"019382754576");
		 sendkeysByJS(driver.findElement(By.name("emailid")),"hue"+Math.floor(Math.random()+100)+"@yopmail.com");
		 sendkeysByJS(driver.findElement(By.name("password")),"abc123!@");
		 //driver.findElement(By.name("sub")).click();
		 //Step4
		 
	  }

}
