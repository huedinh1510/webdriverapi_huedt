package selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;

public class Topic04_MultiSelect_Handle {
	WebDriver driver;
	
	  @BeforeTest
	  public void beforeTest() {
		  driver=new FirefoxDriver();
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.manage().window().maximize();
		  
		 
	  }
	  
	  public void selectMultiOptions(By listboxLocator, By listLocator, String[] expectedoptions, By listSelected) {
		  
		  JavascriptExecutor js=(JavascriptExecutor)driver;
		  js.executeScript("arguments[0].click();", driver.findElement(listboxLocator));
		  WebDriverWait wait=new WebDriverWait(driver,5);
		  wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(listLocator)));
		  List<WebElement> optionList=driver.findElements(listLocator);
		  for(WebElement option:optionList) {
			  for(String expectedoption:expectedoptions) {
				  if(option.getText().equals(expectedoption)) {
					  js.executeScript("arguments[0].scrollIntoView();", option);
					  option.click();
					  System.out.println(expectedoption+" is selected");					 
				  }	
				  List<WebElement> selectedItems=driver.findElements(listSelected);
				  if(selectedItems.size()==expectedoptions.length) {
					  System.out.println("All items have been selected");
					  break;
				  }
			  }
			 
		  }
		 
	  }
	  
	  @Test
	  public void TC01_handleMultiSelectEx1() throws InterruptedException {		
		  System.out.println("Excecise1:");
		  driver.get("http://multiple-select.wenzhixin.net.cn/examples/#basic.html");
		  String[] expectedOption= {"January", "April", "March", "December"};
		  driver.switchTo().frame(0);
		  selectMultiOptions(By.xpath("//button[@class='ms-choice']"),By.xpath("//div[@class='ms-drop bottom']//span"), expectedOption, By.xpath("//div[@class='ms-drop bottom']//li[@class='selected']"));		  
	  }
	  
	  @Test
	  public void TC01_handleMultiSelectEx2() throws InterruptedException {		
		  System.out.println("Excecise2:");
		  driver.get("https://semantic-ui.com/modules/dropdown.html");
		  String[] expectedOption= {"Angular", "HTML", "NodeJS", "React"};
		  selectMultiOptions(By.xpath("//div[@class='ui fluid dropdown selection multiple']//i"),By.xpath("//div[@class='menu transition visible']//div"), expectedOption, By.xpath("//div[@class='ui fluid dropdown selection multiple active visible']//a"));		  
	  }
	  
}
