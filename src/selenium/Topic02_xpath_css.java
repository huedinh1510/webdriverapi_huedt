package selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;

public class Topic02_xpath_css {
	WebDriver driver;
	  @BeforeTest
	  public void beforeTest() {
		  driver=new FirefoxDriver();
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.manage().window().maximize();
		  driver.get("http://live.guru99.com/");
	  }

	  @Test
	  public void TC01_loginEmpty() throws InterruptedException {
		 driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
		 Thread.sleep(2000);
		 driver.findElement(By.id("send2")).click();
		 String errorEmail=driver.findElement(By.id("advice-required-entry-email")).getText();
		 String errorPass=driver.findElement(By.id("advice-required-entry-pass")).getText();
		 Assert.assertEquals(errorEmail, "This is a required field.");
		 Assert.assertEquals(errorPass, "This is a required field.");
	  }
	  
	  @Test
	  public void TC02_loginInvalidEmail() throws InterruptedException {
		  driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
			 Thread.sleep(2000);
			 driver.findElement(By.id("email")).sendKeys("123434234@12312.123123");
			 driver.findElement(By.id("send2")).click();
			 String errorEmail=driver.findElement(By.id("advice-validate-email-email")).getText();			
			 Assert.assertEquals(errorEmail, "Please enter a valid email address. For example johndoe@domain.com.");
			
	  }
	  @Test
	  public void TC03_loginWithPassSmallerThan6() throws InterruptedException {
		  driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
			 Thread.sleep(2000);
			 driver.findElement(By.id("email")).clear();
			 driver.findElement(By.id("email")).sendKeys("automation@gmail.com");
			 driver.findElement(By.id("pass")).sendKeys("123");
			 driver.findElement(By.id("send2")).click();
			 String errorPass=driver.findElement(By.id("advice-validate-password-pass")).getText();
			 Assert.assertEquals(errorPass, "Please enter 6 or more characters without leading or trailing spaces.");
			
	  }
	  @Test
	  public void TC04_loginInvalidPass() throws InterruptedException {
		  driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
			 Thread.sleep(2000);
			 driver.findElement(By.id("email")).clear();
			 driver.findElement(By.id("pass")).clear();
			 driver.findElement(By.id("email")).sendKeys("automation@gmail.com");
			 driver.findElement(By.id("pass")).sendKeys("123123123");
			 driver.findElement(By.id("send2")).click();
			 String errorLogin=driver.findElement(By.xpath("//li[@class=\"error-msg\"]//span")).getText();
			 Assert.assertEquals(errorLogin, "Invalid login or password.");
	  }
	  @Test
	  public void TC05_createNewAccount() throws InterruptedException {
		  driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
			 Thread.sleep(2000);
			 driver.findElement(By.xpath("//a[@title=\"Create an Account\"]")).click();
			 Thread.sleep(2000);
			 driver.findElement(By.id("firstname")).sendKeys("hue");
			 driver.findElement(By.id("middlename")).sendKeys("thi");
			 driver.findElement(By.id("lastname")).sendKeys("dinh");
			 driver.findElement(By.id("email_address")).sendKeys("test"+Math.floor(Math.random()*100)+"@gmail.com");
			 driver.findElement(By.id("password")).sendKeys("12345678");
			 driver.findElement(By.id("confirmation")).sendKeys("12345678");
			 driver.findElement(By.id("is_subscribed")).click();
			 driver.findElement(By.xpath("//button[@title=\"Register\"]")).click();
			 Thread.sleep(3000);
			 String successMsg=driver.findElement(By.xpath("//li[@class=\"success-msg\"]//span")).getText();
			 Assert.assertEquals(successMsg, "Thank you for registering with Main Website Store.");
			 //logout
			 driver.findElement(By.xpath("//div[@class=\"account-cart-wrapper\"]/a")).click();
			 Thread.sleep(1000);
			 driver.findElement(By.xpath("//a[@title=\"Log Out\"]")).click();
			 Thread.sleep(5000);
			 String currentUrl=driver.getCurrentUrl();
			 Assert.assertEquals(currentUrl, "http://live.guru99.com/index.php/");
			 
	  }
	  


}
